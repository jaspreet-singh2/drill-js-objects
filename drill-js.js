// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.
// Check and use MDN as well

/*
    Create a function for each problem in a file called
        keys.js
        values.js
        pairs.js
    and so on in the root of the project.

    Ensure that the functions in each file is exported and tested in its own file called
        testKeys.js
        testValues.js
        testPairs.js
    and so on in a folder called test.

    Create a new git repo on gitlab for this project, ensure that you commit after you complete each problem in the project.
    Ensure that the repo is a public repo.

    When you are done, send the gitlab url to your mentor
*/

// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys

function keys(obj) {
  var list = [];
  for (key in obj) {
    list.push(key);
  }
  return list;
}

// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

function values(obj) {
  var keysL = keys(obj);
  var values = [];
  for (i = 0; i < keysL.length; i++) {
    values.push(obj[keysL[i]]);
  }
  return values;
}
// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

function mapObject(obj, cb) {
  for (key in obj) {
    obj[key] = cb(obj[key]);
  }
  
}

// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

function pairs(obj) {
  var pairs = [];
  for (key in obj) {
    pairs.push([key, obj[key]]);
  }
  return pairs;
}

/* STRETCH PROBLEMS */
// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

function invert(obj) {
  var invertedObj = {};
  for (key in obj) {
    invertedObj[obj[key]] = key;
  }
  return invertedObj;
}
// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

function defaults(obj, defaultProps) {
  for (key in defaultProps) {
    if (obj[key] === undefined) obj[key] = defaultProps[key];
  }
  return obj;
}

exports.modules = { keys, values, pairs, invert, defaults, mapObject };
