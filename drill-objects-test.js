var objectFunc = require("./drill-js").modules;

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions

console.log('\x1b[32m%s\x1b[0m','function keys(obj)') 
// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys

objectFunc.keys(testObject).forEach((element) => {
  console.log(element);
});

console.log('\x1b[32m%s\x1b[0m','function values(obj)') 
// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

objectFunc.values(testObject).forEach((element) => {
  console.log(element);
});

console.log('\x1b[32m%s\x1b[0m','function mapObject(obj, cb)' )
// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

objectFunc.mapObject(testObject, (x) => x + 10);
objectFunc.pairs(testObject).forEach((element) => {
  console.log(element);
});

console.log('\x1b[32m%s\x1b[0m','function pairs(obj)')
// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

objectFunc.pairs(testObject).forEach((element) => {
  console.log(element);
});

/* STRETCH PROBLEMS */

console.log('\x1b[32m%s\x1b[0m','function invert(obj)' )
// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

var invertedObj = objectFunc.invert(testObject);
objectFunc.pairs(invertedObj).forEach((element) => {
  console.log(element);
});

console.log('\x1b[32m%s\x1b[0m','function defaults(obj, defaultProps)' )
// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

var defaultObj = objectFunc.defaults(testObject, { 'second_name'
  : 'Batman', 'Has_Bike': true });
objectFunc.pairs(defaultObj).forEach((element) => {
  console.log(element);
});
